import F7 from '../node_modules/framework7/framework7-lite.esm.bundle'

// Import Framework7-Vue Plugin
import F7Vue from '../node_modules/framework7-vue/framework7-vue.esm.bundle.js'

// Import Framework7 Styles
import '../node_modules/framework7/css/framework7.bundle.css'

// Import Icons and App Custom Styles
import '../src/css/icons.css'
import '../src/css/app.css'

// Init Framework7-Vue Plugin
// NOTE: this does not involve Vue directly at all, so some other configuration must be needed...
// unless F7.use() is calling Vue.use() as a side effect?
F7.use(F7Vue)

// =========================================
// Default configuration for storybook
// =========================================
import {addParameters} from '@storybook/vue'

// addParameters is passing parameters to the app root which is a F7 component... but what registered the F7 components into Vue in the first place?
addParameters({
  f7params: {
    name: 'login-framework7', // App name
    theme: 'auto', // Automatic theme detection
    // App root data
    data: function() {
      return {}
    },

    // App routes
    // routes: routes,
  },
})
