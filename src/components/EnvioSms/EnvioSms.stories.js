import EnvioSms from "./EnvioSms";

export default {
    title: 'Pantalla Envio SMS'
  };

export const Default = () => ({
  components: { EnvioSms },
  template: `<envio-sms />`
});